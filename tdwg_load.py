# -*- coding: utf-8 -*-
"""This script reads in the TDWG region codes, as downloaded from:
http://www.tdwg.org/standards/109
(Edition 2)

It also uses an ISO index, available from:
http://www.iso.org/iso/country_codes/iso_3166_code_lists.htm

If making changes to the structure of the regions processed here,
you may need to comment out the final part of taxonome/regions.py, so
that it doesn't attempt to load the previous pickle when it's imported.
"""
from taxonome import regions
import pickle, os
os.chdir("regions_data")

ISOfile = open("ISO_countrycodes.txt",encoding="iso8859_1")
ISOfile.readline()
ISOfile.readline()
ISO_names = {}
for line in ISOfile:
    fields = line.strip().split(";")
    ISO_names[fields[0]] = fields[1]

world = regions.Region("World")
names = {}
ISO = {}
tdwg_levels = [None] + [{} for i in range(4)]

L1file = open("tblLevel1.txt",encoding="iso8859_15")
L1file.readline()
for line in L1file:
    fields = line.strip().split("*")
    code = fields[0].partition(",")[0]
    region = regions.Region(fields[1], code)
    world.children.add(region)
    names[fields[1]] = region
    tdwg_levels[1][code] = region
    
L2file = open("tblLevel2.txt",encoding="iso8859_15")
L2file.readline()
for line in L2file:
    fields = line.strip().split("*")
    code = fields[0].partition(",")[0]
    name = fields[1]
    region = regions.Region(name, code)
    parentcode = fields[2].partition(",")[0]
    tdwg_levels[1][parentcode].children.add(region)
    names[name] = region
    if name.upper() in ISO_names:
        ISO[ISO_names[name.upper()]] = region
    tdwg_levels[2][code] = region
        
L3file = open("tblLevel3.txt",encoding="iso8859_15")
L3file.readline()
L3groups = {}
for line in L3file:
    fields = line.strip().split("*")
    name = fields[1]
    code = fields[0]
    region = regions.Region(name, code)
    parentcode = fields[2].partition(",")[0]
    tdwg_levels[2][parentcode].children.add(region)
    names[name] = region
    if name.upper() in ISO_names:
        ISO[ISO_names[name.upper()]] = region
    tdwg_levels[3][code] = region
        
L4file = open("tblLevel4.txt",encoding="iso8859_15")
L4file.readline()
L4groups = {}
for line in L4file:
    fields = line.strip().split("*")
    name = fields[1]
    code = fields[0]
    region = regions.Region(name, code)
    if fields[5]:
        region.notes = fields[5]
    tdwg_levels[3][fields[2]].children.add(region)
    names[name] = region
    if name.upper() in ISO_names:
        ISO[ISO_names[name.upper()]] = region
    tdwg_levels[4][code] = region

# Where a level 3 region has just one child, the name
# should point to the level 3 region, not the level 4 one.
for code, region in tdwg_levels[3].items():
    if len(region.children) == 1:
        names[region.name] = region

tdwg = regions.CombinedIndex(tdwg_levels[1:])

# --- Some extras: -------------------------
def makecountry(name, tdwgcodeslist):
    country = regions.Region(name)
    for code in tdwgcodeslist:
        country.children.add(tdwg[code])
    names[name] = country
    ISO[ISO_names[name.upper()]] = country

makecountry("Russian Federation", ["RUC","RUE","RUN","RUS","RUW","NCS","30","31"])
names["Russia"] = names["Russian Federation"]

makecountry("Turkey", ["TUE","TUR"])
makecountry("India", ["IND","WHM","EHM-SI","EHM-DJ","EHM-AP","ASS","LDV"])
makecountry("United Kingdom", ["GRB","IRE-NI"])
# The US here is only the "lower 48"--excluding Alaska and Hawai'i
makecountry("United States", ["78","75","74","77","76","73"])
makecountry("Canada", ["71","72","NWT","YUK","NUN"])
makecountry("Chile", ["CLC","CLN","CLS"])
makecountry("South Africa", ["TVL","CPP","OFS","NAT"])
makecountry("Malaysia", ["MLY-PM","BOR-SB","BOR-SR"])
makecountry("Argentina", ["AGE","AGW","AGS"])
makecountry("Indonesia", ["LSI-BA","LSI-LS","BOR-KA","SUL","JAW",\
                            "SUM","MOL","NWG-IJ"])

ISO["VE"] = names["Venezuela"]
ISO["TZ"] = names["Tanzania"]
ISO["TW"] = names["Taiwan"]
ISO["LY"] = names["Libya"]
ISO["IR"] = names["Iran"]
ISO["KR"] = names["South Korea"]
ISO["KP"] = names["North Korea"]
ISO["LA"] = names["Laos"]
ISO["FK"] = tdwg["FAL"]
ISO["BO"] = names["Bolivia"]
ISO["SJ"] = names["Svalbard"]
ISO["BA"] = names["Bosnia-Herzegovina"]
ISO["MK"] = names["Macedonia"]
ISO["SY"] = names["Syria"]
ISO["MD"] = names["Moldova"]
ISO["BN"] = names["Brunei"]
ISO["VN"] = names["Vietnam"]
names["Kyrgyzstan"] = names["Kirgizistan"]
ISO["KG"] = names["Kirgizistan"]
names["Macao"] = names["Macau"]
ISO["MO"] = names["Macau"]
names["Panama"] = names["Panamá"]
ISO["PA"] = names["Panamá"]
names["Burkina Faso"] = names["Burkina"]
ISO["BF"] = names["Burkina"]
names["Gambia"] = names["Gambia, The"]
ISO["GM"] = names["Gambia, The"]
names["Côte d'Ivoire"] = names["Ivory Coast"]
ISO["CI"] = names["Ivory Coast"]
names["Trinidad and Tobago"] = names["Trinidad-Tobago"]
ISO["TT"] = names["Trinidad-Tobago"]
names["Antigua and Barbuda"] = names["Antigua-Barbuda"]
ISO["AG"] = names["Antigua-Barbuda"]
names["Tajikstan"] = names["Tadzhikistan"]
ISO["TJ"] = names["Tadzhikistan"]
names["Democratic Republic of the Congo"] = names["Zaire"]
ISO["CD"] = names["Zaire"]

print(world.children)
#for name, code in ISO_names.items():
    #if not code in ISO:
        #print(name, code)

pickle.dump((world, tdwg_levels, names, ISO), open("../taxonome/TDWG_regions.pkl","wb"))