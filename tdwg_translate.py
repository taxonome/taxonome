# -*- coding: utf-8 -*-
"""This script reads in the TDWG region codes, as downloaded from:
http://www.tdwg.org/standards/109
(Edition 2)

It also uses an ISO index, available from:
http://www.iso.org/iso/country_codes/iso_3166_code_lists.htm
"""
from taxonome import regions
import pickle, os
os.chdir("regions_data")
outfile = open("tdwg_data.py","w")
outfile.write("""# -*- coding: utf-8 -*-
\"""This contains data for the TDWG world regions. Do not use this module
directly, but instead do:
from taxonome import regions
regions.names["China"]
regions.tdwg["NET"]
regions.tdwg_levels[1]["1"]

If load-tdwg-regions is false in settings, these can be loaded with:
regions.load_tdwg()

Data is as downloaded from:
http://www.tdwg.org/standards/109
(Edition 2)

It also uses an ISO index, available from:
http://www.iso.org/iso/country_codes/iso_3166_code_lists.htm
\"""
data = [\\
""")

ISOfile = open("ISO_countrycodes.txt",encoding="iso8859_1")
ISOfile.readline()
ISOfile.readline()
ISO_names = {}
for line in ISOfile:
    fields = line.strip().split(";")
    ISO_names[fields[0]] = fields[1]

world = regions.Region("World")
names = {}
ISO = {}
tdwg_levels = [None] + [set() for i in range(4)]

outfile.write("# TDWG level 1:\n")
L1file = open("tblLevel1.txt",encoding="iso8859_15")
L1file.readline()
for line in L1file:
    fields = line.strip().split("*")
    code = fields[0].partition(",")[0]
    outfile.write("('{0}','{1}',{2},'{3}', None),\n".format(code, fields[1], None, "!!WORLD"))
    tdwg_levels[1].add(code)
    
outfile.write("\n# TDWG level 2:\n")
L2file = open("tblLevel2.txt",encoding="iso8859_15")
L2file.readline()
for line in L2file:
    fields = line.strip().split("*")
    code = fields[0].partition(",")[0]
    name = fields[1]
    parentcode = fields[2].partition(",")[0]
    if name.upper() in ISO_names:
        ISOname = "'{0}'".format(ISO_names[name.upper()])
    else:
        ISOname = None
    outfile.write("('{0}','{1}',{2},'{3}', None),\n".format(code, name, ISOname, parentcode))
    region = regions.Region(name, code)
    tdwg_levels[2].add(code)

outfile.write("\n# TDWG level 3:\n")
L3file = open("tblLevel3.txt",encoding="iso8859_15")
L3file.readline()
L3groups = {}
for line in L3file:
    fields = line.strip().split("*")
    name = fields[1]
    code = fields[0]
    region = regions.Region(name, code)
    parentcode = fields[2].partition(",")[0]
    if name.upper() in ISO_names:
        ISOname = "'{0}'".format(ISO_names[name.upper()])
    else:
        ISOname = None
    outfile.write("('{0}','{1}',{2},'{3}', None),\n".format(code, name, ISOname, parentcode))
    tdwg_levels[3].add(code)

outfile.write("\n# TDWG level 4:\n")
L4file = open("tblLevel4.txt",encoding="iso8859_15")
L4file.readline()
L4groups = {}
for line in L4file:
    fields = line.strip().split("*")
    name = fields[1]
    code = fields[0]
    notes = fields[5]
    parentcode = fields[2]
    if name.upper() in ISO_names:
        ISOname = "'{0}'".format(ISO_names[name.upper()])
    else:
        ISOname = None
    outfile.write("('{0}',\"{1}\",{2},'{3}','{4}'),\n".format(code, name, ISOname, parentcode, notes))
    tdwg_levels[4].add(code)
    
outfile.write("]\n\n")
outfile.write("tdwg_by_level = " + repr(tdwg_levels))
outfile.close()
