# Extract names and distribution information from the Kew grass synonymy
# database, found at:
# http://www.kew.org/data/grasses-syn/

# Run this script with the path to the MDB database downloaded from Kew:
#
#     ./extract_grasssyn.sh grassFlora.mdb

# This script requires 'MDB Tools' (http://mdbtools.sourceforge.net/), which at
# the time of writing is available for Linux and Mac OS X.

mdb-export $1 NAMES > kew_grass_names.csv
mdb-export $1 AREAS > kew_grass_distrib.csv
mdb-export $1 DeltaSp > kew_grass_pages.csv

# These patches resolve a handful of errors in the database. They were made
# against the version of the database dated 20th February 2011 on Kew's website.
# If they fail to apply with newer versions, you will need to either update the
# patches, or comment out the lines below to skip them.
patch kew_grass_names.csv < kew_grass_names_cleaning.patch
patch kew_grass_distrib.csv < kew_grass_distrib_cleaning.patch

python3 kew_build_db.py Kew_grass.jsonlines kew_grass_names.csv \
                        kew_grass_distrib.csv kew_grass_pages.csv 
            
