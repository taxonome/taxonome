#!/usr/bin/python3
# coding: utf-8
"""Build the Taxonome file from CSV files extracted from the grass synonymy
database.

Call with command line arguments:
1. The name of the target database
2. The CSV file of synonymy (the NAMES table in Kew's database)
3. The CSV file of distribution (the AREAS table in Kew's database)
4. The CSV file of Grassbase pages (the DeltaSp table in Kew's database)
"""
import csv
import sys

import taxonome
from taxonome.taxa.file_jsonlines import save_taxa

kew_grasses_file = open(sys.argv[1], "w")
holding = {}
names_in = csv.DictReader(open(sys.argv[2]))
distrib_in = csv.reader(open(sys.argv[3]))
pages_in = csv.reader(open(sys.argv[4]))

hybridmap = {"V":"Varieties","S":"Species","G":"Genera"}
synonymstatuses = {"B","C","H","O","V"}
uncertain_statuses = {"X", "Z"}

synonym_rows = []
for details in names_in:
    # Seemingly the database isn't completely sanitised.
    details["Keyname"] = details["Keyname"].strip()
    details["Accepted"] = details["Accepted"].strip()
    
    
    # Handle accepted names first: we'll come back to synonyms
    if details["Status"] in synonymstatuses \
                        or details["Keyname"] != details["Accepted"]:
        assert details["Status"] != "A", details["Keyname"]
        synonym_rows.append(details)
        continue
    
    if details["BasAuthor"]:
        author = details["BasAuthor"] + " " + details["Author"]
    else:
        author = details["Author"]
    taxon = taxonome.Taxon(details["Accepted"], author)
    
    # Fill in details
    if details['Hybrid']:
        taxon.info["Hybrid"] = hybridmap[details['Hybrid']]
    if details['Reference']:
        taxon.info["Reference"] = details['Reference']
        taxon.info["Date"] = details['Date']
    if details['Notes']:
        taxon.info['Note'] = details['Notes']
        
    # For checking below
    taxon.info["Status"] = details["Status"]
    
    #print(taxon.name)  #DEBUG
    # Duplicates may arise where an 'appl incert' name conflicts with an
    # accepted one. In this case, throw away the uncertain one. 
    if taxon.name.plain in holding and details["Status"] in uncertain_statuses:
        continue
    assert taxon.name.plain not in holding \
            or holding[taxon.name.plain].info["Status"] in uncertain_statuses,\
            taxon.name.plain
    holding[taxon.name.plain] = taxon
        
print("Adding synonyms...")
for details in synonym_rows:
    if details['Keyname'] == details['Accepted']:
        # This can happen where a homonym has not been reassigned a new name,
        # e.g. Aegilops uniaristata. Ignore for now.
        continue
    if details["BasAuthor"]:
        author = details["BasAuthor"] + " " + details["Author"]
    else:
        author = details["Author"]
    name = taxonome.Name(details["Keyname"], author)
    holding[details["Accepted"]].othernames.add(name)

print("Reading distribution data...")
next(distrib_in)   # Throw away header row
for keyname, region, country in distrib_in:
    holding[keyname].distribution.update(country.split())
        
print("Reading page data...")
next(pages_in)     # Throw away header row.
spare_pages = []
for keyname, url in pages_in:
    try:
        holding[keyname].info["page"] = url.partition("#")[0]
    except KeyError:
        spare_pages.append(keyname)

if spare_pages:
    print("Pages without matching taxa:", ", ".join(spare_pages))

print("Saving...")
for taxon in holding.values():
    # We no longer need the status, that was just for checking
    status = taxon.info.pop("Status")
    if status in uncertain_statuses:
        taxon.info["Status"] = "Uncertain"

save_taxa(kew_grasses_file, holding.values())
