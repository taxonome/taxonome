#!/usr/bin/env python3
# coding: utf-8
"""Extract data from the Pages.dtx file in the ILDIS explorer download.

Run this script with the path to the Pages.dtx file, e.g.::

    python3 ildis_extract.py Pages.dtx
"""
import struct
import sys
import re

import taxonome
from taxonome.taxa.file_jsonlines import save_taxon
from taxonome.taxa.base import UncertainSpeciesError

pagesfile = open(sys.argv[1],"rb")
pagesfile.read(2)
destination = open("ILDIS.jsonlines", "w")

statusre = re.compile(r"\(([a-zA-Z])\)")

# Countries where we don't want to get the subdivisions.
do_not_subdivide = {'Kazakhstan', 'Kirgizstan', 'Tadzhikistan', 'Mongolia',
            'Turkmenistan', 'Uzbekistan', 'Ukraine', 'Gruzia'}
# Improve names to better match TDWG.
replace_names = {"New Zealand(North)":"New Zealand North", 
"New Zealand(South)":"New Zealand South",
"Distrito Federal(Brazil)":"Brazilia Distrito Federal",
"Tierra del Fuego(Argentina)": 'Tierra del Fuego (Argentina)',
"Distrito Federal(Mexico)": "Mexico Distrito Federal",
"Gambia The": "Gambia, The",
}

def sget(fmt, fileobj):
    """Read enough bytes from fileobj to match the specified format, and return
    the values unpacked from those bytes."""
    fmt = "<" + fmt
    buf = fileobj.read(struct.calcsize(fmt))
    return struct.unpack(fmt, buf)

def striprefs(txt):
    while "{" in txt and "}" in txt:
        txt = txt[:txt.find("{")] + txt[txt.find("}")+1:]
    return txt.strip()

def add_to_distrib(taxon, rname, status):
    rname = replace_names.get(rname, rname)
    if status == 'N':
        taxon.distribution.add(rname)
    else:
        taxon.extra_distribution.add(rname)

count = 0
while True:
    count += 1
    if count % 1000 == 0:
        print(int(count/1000), "k", sep="", end=" ")
        sys.stdout.flush()
    
    numfields, = sget("i",pagesfile)
    taxonfields = {}
    for i in range(numfields):
        l, = sget("i", pagesfile)
        fieldname, nlines = sget(str(l)+"s i", pagesfile)
        fieldval = []
        for j in range(nlines):
            in1, in2, colour, l = sget("4i", pagesfile)
            if fieldname == b"Distribution" and j>1 and in1 > prev_indent:
                fieldval.pop()
            fieldval.append(sget(str(l)+"s", pagesfile)[0].decode("cp850"))
            prev_indent = in1
        taxonfields[fieldname.decode("cp850")] = fieldval[1:]
    pagesfile.read(4) #AnJz
    
    numrefs, = sget("i",pagesfile)
    if numrefs:
        taxonfields["refs"] = []
    for i in range(numrefs):
        mystery, l = sget("2i", pagesfile)
        authors, year, l = sget(str(l)+"s 2i", pagesfile)
        title, = sget(str(l)+"s", pagesfile)
        ref = {"authors":authors.decode("cp850"), "year":year,\
                "title":title.decode("cp850")}
        taxonfields["refs"].append(ref)
        
    accname = taxonfields.pop("Accepted Name")[0].partition(" {")[0]
    accname = accname.replace(" (provisionally accepted)", "")
    #print(accname) #DEBUG
    taxon = taxonome.Taxon(taxonome.Name.from_string(accname))
    
    if "Synonyms" in taxonfields:
        for syn in taxonfields.pop("Synonyms"):
            syn = syn.partition(" {")[0].replace(" (misapplied)", "")\
                                        .replace(" (orthographic variant)", "")
            try:
                synname = taxonome.Name.from_string(syn)
            except UncertainSpeciesError:
                continue
            
            if synname.rank != "genus":
                taxon.othernames.add(synname)
            
    taxon.extra_distribution = set()
    if "Distribution" in taxonfields:
        for region in taxonfields.pop("Distribution"):
            region = striprefs(region)
            statusbit = statusre.search(region)
            if not statusbit:
                taxon.distribution.add(region)
                continue
            ixbrack = statusbit.start()
            status = statusbit.group(1)
            rname = region[:ixbrack -1]
            
            # Break out specified subregions:
            if (": " in region) and (rname not in do_not_subdivide):
                for subreg in region.partition(": ")[2].strip("()").split(", "):
                    add_to_distrib(taxon, subreg.strip().rstrip("*"), status)
            
            # Take the whole country as one.
            else:
                add_to_distrib(taxon, rname, status)
    
    if "References" in taxonfields:
        del taxonfields["References"]
    
    if "Life Form" in taxonfields:
        lf = taxonfields["Life Form"][0].split("; ")
        taxonfields["Life Form"] = [a.partition(" {")[0] for a in lf]
    
    taxon.info = taxonfields
    
    # Actually save to disk
    save_taxon(destination, taxon)
    
    pagesfile.read(4)
    # Of these three fields, the first seems to be indexing references,
    # the second is mapping the distribution (country number, native-ness).
    # The third usually seems to be empty.
    while True:
        if pagesfile.read(1) == b"J":
            if pagesfile.read(1) == b"z":
                break
    l, = sget("i",pagesfile)
    pagesfile.read(l*5)
    pagesfile.read(4)
    l, = sget("i",pagesfile)
    if l != 0:
        # This should never happen
        print(l, taxon)
        assert False
    if len(pagesfile.read(4)) < 4:
        break
        
pagesfile.close()
print() # New line
