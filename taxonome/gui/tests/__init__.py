# Ensure that we've got an application when we run the tests
import sys
from PyQt4.QtGui import QApplication
app = QApplication.instance() or QApplication(sys.argv)
