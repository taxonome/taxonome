# encoding: utf-8
import unittest

from taxonome.gui import selectname
from taxonome import Taxon, TaxonSet, Name

class GuiNameSelectorTests(unittest.TestCase):
    def setUp(self):
        self.ts = TaxonSet()
        self.liquorice = Taxon("Glycyrrhiza glabra", "L.")
        self.liquorice.othernames.add(Name("Glycyrrhiza glandulifera", "Waldst. and Kit."))
        self.ts.add(self.liquorice)
        ch_liquorice = Taxon("Glycyrrhiza uralensis", "Fisch.")
        ch_liquorice.othernames.add(Name("Glycyrrhiza glandulifera", "Ledeb."))
        self.ts.add(ch_liquorice)
        
        self.nameselector = selectname.GuiNameSelector(previous_choices={})
    
    def _set_results(self, results):
        """Prepare the nameselector to return the specified result triple."""
        assert len(results) == 3
        def choose(name_options, name):
            self.nameselector.choice_results.put(results)
        self.nameselector.choice_required.connect(choose)
        return choose
    
    def test_try_new_name(self):
        self._set_results((True, None, "Glycyrrhiza madeup"))
        with self.assertRaises(KeyError):
            self.ts.select("Glycyrrhiza glandulifera", nameselector=self.nameselector)
