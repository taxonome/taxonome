# encoding: utf-8
import unittest

from taxonome.gui import csvdialogs
from .utils import write_named_tempfile

example_csv_1 = """Name,Authority,中文
Thymus vulgaris,L.,銀斑百里香
Ocimum basilicum,L.,罗勒
Petroselinum crispum,(Mill.) Fuss,香芹"""

class CsvImportDialogTests(unittest.TestCase):
    def test_read_file(self):
        with write_named_tempfile(example_csv_1) as filename:
            dialog = csvdialogs.CsvImportDialog(filename)
            self.assertEqual(dialog.ui.namefield.count(), 3)
            self.assertEqual(dialog.ui.namefield.itemText(2), "中文")
    
    def test_read_file_gb18030(self):
        ix_gb18030 = csvdialogs.supported_encodings.index('GB18030')
        with write_named_tempfile(example_csv_1, encoding='GB18030') as filename:
            dialog = csvdialogs.CsvImportDialog(filename)

            self.assertNotEqual(dialog.ui.namefield.itemText(2), "中文")
            self.assertEqual(dialog.ui.preview.rowCount(), 3)
            self.assertEqual(dialog.ui.preview.columnCount(), 3)
            self.assertNotEqual(dialog.ui.preview.item(0, 2).text(), '銀斑百里香')
            
            # Set the desired encoding
            dialog.ui.csv_encoding.setCurrentIndex(ix_gb18030)
            self.assertEqual(dialog.ui.namefield.count(), 3)
            self.assertEqual(dialog.ui.namefield.itemText(2), "中文")
            self.assertEqual(dialog.ui.preview.rowCount(), 3)
            self.assertEqual(dialog.ui.preview.columnCount(), 3)
            self.assertEqual(dialog.ui.preview.item(0, 2).text(), '銀斑百里香')


example_csv_syn_1 = """Name,Authority,Accepted name,Accepted authority,中文
Thymus vulgaris,L.,Thymus vulgaris,L.,銀斑百里香
Ocimum basilicum,L.,Ocimum basilicum,L.,罗勒
Petroselinum crispum,(Mill.) Fuss,Petroselinum crispum,(Mill.) Fuss,香芹
Apium crispum,Mill.,Petroselinum crispum,(Mill.) Fuss,香芹
Apium petroselinum,L.,Petroselinum crispum,(Mill.) Fuss,香芹"""

class CsvSynonymsImportDialogTests(unittest.TestCase):
    def test_read_file(self):
        with write_named_tempfile(example_csv_syn_1) as filename:
            dialog = csvdialogs.CsvSynonymsImportDialog(filename)
            self.assertEqual(dialog.ui.accnamefield.count(), 5)
            self.assertEqual(dialog.ui.accnamefield.itemText(4), "中文")
    
    def test_read_file_gb18030(self):
        ix_gb18030 = csvdialogs.supported_encodings.index('GB18030')
        with write_named_tempfile(example_csv_syn_1, encoding='GB18030') as filename:
            dialog = csvdialogs.CsvSynonymsImportDialog(filename)

            # With incorrect encoding
            self.assertEqual(dialog.ui.accnamefield.count(), 5)
            self.assertNotEqual(dialog.ui.accnamefield.itemText(4), "中文")
            self.assertEqual(dialog.ui.preview.rowCount(), 5)
            self.assertEqual(dialog.ui.preview.columnCount(), 5)
            self.assertNotEqual(dialog.ui.preview.item(0, 4).text(), '銀斑百里香')
            
            # Set the desired encoding
            dialog.ui.csv_encoding.setCurrentIndex(ix_gb18030)
            self.assertEqual(dialog.ui.accnamefield.count(), 5)
            self.assertEqual(dialog.ui.accnamefield.itemText(4), "中文")
            self.assertEqual(dialog.ui.preview.rowCount(), 5)
            self.assertEqual(dialog.ui.preview.columnCount(), 5)
            self.assertEqual(dialog.ui.preview.item(0, 4).text(), '銀斑百里香')

if __name__ == "__main__":
    unittest.main()
