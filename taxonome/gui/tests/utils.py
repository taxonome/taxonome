from contextlib import contextmanager
import os.path
import tempfile

@contextmanager
def write_named_tempfile(contents, name='file.txt', encoding='utf-8'):
    """A context manager for a named temporary file. Unlike the stdlib
    NamedTemporaryFile, this is closed, so it can be opened by name in other
    code.
    
    The context manager gives the full path to the file, not a file object.
    """
    with tempfile.TemporaryDirectory() as tdir:
        path = os.path.join(tdir, name)
        with open(path, 'w', encoding=encoding) as f:
            f.write(contents)
        
        yield path
