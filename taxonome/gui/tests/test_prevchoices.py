#encoding: utf-8
import unittest

from taxonome.gui import prevchoices
from taxonome import Name

gg = Name("Glycyrrhiza glandulifera")
gg1 = Name("Glycyrrhiza glandulifera", "Ledeb.")
gu = Name("Glycyrrhiza uralensis", "Fisch.")
gg2 = Name("Glycyrrhiza glandulifera", "Waldst. & Kit.")
gglab = Name("Glycyrrhiza glabra", "L.")

class PrevChoicesDialogTests(unittest.TestCase):
    def setUp(self):
        self.choices = {repr(gg): ([(gg1, gu), (gg2, gglab)], (gg1, gu))}
        self.dialog = prevchoices.PrevChoicesDialog(self.choices)
        self.prevchoice = self.dialog.ambignames_model.item(0)
        ix = self.dialog.ambignames_model.indexFromItem(self.prevchoice)
        self.dialog.ui.ambignames.setCurrentIndex(ix)
        self.dialog.select_ambigname(ix)
    
    def test_display(self):
        self.assertEqual(self.dialog.ambignames_model.rowCount(), 1)
        self.assertEqual(self.prevchoice.name, gg)
        self.assertEqual(self.dialog.options_model.rowCount(), 2)
        option1 = self.dialog.options_model.item(0)
        self.assertEqual(option1.namepair, (gg1, gu))
        option2 = self.dialog.options_model.item(1)
        self.assertEqual(option2.namepair, (gg2, gglab))
        self.assertIn("uralensis", self.dialog.ui.chosenmatch.text())
    
    def test_forget_all(self):
        self.dialog.forget_all()
        self.assertEqual(len(self.choices), 0)
        self.assertEqual(self.dialog.ambignames_model.rowCount(), 0)
    
    def test_forget(self):
        self.dialog.forget()
        self.assertEqual(len(self.choices), 0)
        self.assertEqual(self.dialog.ambignames_model.rowCount(), 0)
    
    def test_update_choice(self):
        ix = self.dialog.options_model.createIndex(1, 0)
        self.dialog.ui.altmatches.setCurrentIndex(ix)
        self.dialog.update_choice()
        self.assertEqual(self.choices[repr(gg)][1][1], gglab)
        self.assertIn("glabra", self.dialog.ui.chosenmatch.text())
    
    def test_reject_all(self):
        self.dialog.reject_all()
        self.assertIsInstance(self.choices[repr(gg)][1], KeyError)
        self.assertIn("Reject all", self.dialog.ui.chosenmatch.text())
