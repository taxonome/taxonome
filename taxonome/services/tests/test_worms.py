import unittest
from nose.plugins.attrib import attr

from taxonome import Name, Taxon
from taxonome.services import worms

@attr('http')
class GRINTestCase(unittest.TestCase):
    def setUp(self):
        self.resource = worms.WoRMSTaxaResource()
    
    def test_select(self):
        ss = self.resource.select("Solea solea")
        self.assertEqual(ss.name.plain, "Solea solea")
        self.assertIn('127160', ss.url)
        assert len(ss.othernames) > 0, len(ss.othernames)
        assert any(n.plain == "Pleuronectes solea" for n in ss.othernames)
        
        # Select using a synonym
        ss2 = self.resource.select("Pleuronectes solea")
        self.assertEqual(ss2.name.plain, "Solea solea")
        self.assertIn('127160', ss2.url)
