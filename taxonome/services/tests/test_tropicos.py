import unittest
from nose.plugins.attrib import attr
from nose import SkipTest

from taxonome import Name, Taxon, TaxonSet
from taxonome.services import tropicos

@attr('http')
class GRINTestCase(unittest.TestCase):
    def test_select(self):
        try:
            r = tropicos.select("Zea mays")
        except tropicos.TropicosNoAPIKey:
            raise SkipTest("Need Tropicos API key to run this")
        self.assertEqual(r.name.plain, "Zea mays")
        self.assertEqual(str(r.name.authority), "L.")
        self.assertEqual(r.id, '25510055')

