import unittest
from nose.plugins.attrib import attr

from taxonome import Name, Taxon, TaxonSet
from taxonome.services import grin

@attr('http')
class GRINTestCase(unittest.TestCase):
    def test_select(self):
        r = grin.select("Zea mays")
        self.assertEqual(r.name.plain, "Zea mays")
        self.assertEqual(str(r.name.authority), "L.")
        self.assertEqual(r.id, '42207')
    
    def test_fetch(self):
        r = grin.fetch("Restionaceae")
        assert isinstance(r, TaxonSet), r
        self.assertIn("Restio amblycoleus", r)
        ra = r.select("Restio amblycoleus")
        self.assertEqual(ra.name.plain, "Chordifex amblycoleus")
        self.assertEqual(ra.id, '464712')
