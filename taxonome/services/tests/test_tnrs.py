import unittest
from nose.plugins.attrib import attr

from taxonome import Name, Taxon
from taxonome.services import tnrs

@attr('http')
class TNRSTestCase(unittest.TestCase):
    def test_select(self):
        n = "Zea mays"
        r = tnrs.select(n)
        self.assertEqual(r.name.plain, n)
        self.assertEqual(str(r.name.authority), "L.")
        self.assertEqual(r.info['family'], "Poaceae")
        
        n = Name("Glycyrrhiza glabra", "L.")
        r = tnrs.select(n)
        self.assertEqual(r.name, n)
        self.assertEqual(r.info['family'], "Fabaceae")
    
    def test_match_names(self):
        names = [Name("Glycyrrhiza glabra", "L."),
                 Name("Zea mays", "L."),
                 Name("Vulpia persica", "(Boissier & Buhse) V. I. Kreczetowicz & Bobrov"),
                 Name("euienkj","foo")]
        
        def checkres(res):
            for name, match in res:
                assert isinstance(name, Name), name
                assert isinstance(match, Taxon), match
        
        # Drop unmatched names
        res = list(tnrs.match_names(names, include_unmatched=False))
        self.assertEqual(len(res), 3)
        checkres(res)
        
        # Keep unmatched names
        res = list(tnrs.match_names(names, include_unmatched=True))
        self.assertEqual(len(res), 4)
        checkres(res[:-1])
            
        name, match = res[-1]
        assert isinstance(name, Name), name
        self.assertIs(match, None)
