from __future__ import print_function

import os
import sys

def outdated(target, dependencies):
    if not os.path.exists(target):
        return True
    mtime = os.stat(target).st_mtime
    if isinstance(dependencies, str):
        dependencies = [dependencies]
    return any(os.stat(f).st_mtime > mtime for f in dependencies)

def helper(cmds):
    def help(*argv):
        if not argv:
            print("Available commands:", *cmds, sep="\n  ")
        else:
            print(cmds[argv[0]].__doc__)
    return help

def multicaller(funcs):
    if hasattr(funcs, '__call__'):
       return funcs
    
    def call(*args):
        for func in funcs:
            func(*args)
    call.__doc__ = "Runs the functions: %s" % ", ".join(f.__name__ for f in funcs)
    return call

def main(*namecmds, **cmds):
    cmds = dict((n, multicaller(c)) for n,c in cmds.items())
    for cmd in namecmds:
        cmds[cmd.__name__] = cmd
    
    if 'help' not in cmds:
        cmds['help'] = helper(cmds)
    
    if len(sys.argv) == 1:
        cmds['default']()
    
    else:
        cmds[sys.argv[1]](**sys.argv[2:])
