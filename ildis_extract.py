#!/usr/bin/env python3
import taxonome
import struct
import re
statusre = re.compile(r"\(([a-zA-Z])\)")

def sget(fmt, fileobj):
    fmt = "<" + fmt
    buf = fileobj.read(struct.calcsize(fmt))
    return struct.unpack(fmt, buf)

def striprefs(txt):
    while "{" in txt and "}" in txt:
        txt = txt[:txt.find("{")] + txt[txt.find("}")+1:]
    return txt.strip()
    
pagesfile = open("Pages.dtx","rb")
pagesfile.read(2)
taxa = taxonome.taxa.TaxonShelf("ildis-names","ildis-taxa")
while True:
    numfields, = sget("i",pagesfile)
    taxonfields = {}
    for i in range(numfields):
        l, = sget("i", pagesfile)
        fieldname, nlines = sget(str(l)+"s i", pagesfile)
        fieldval = []
        for j in range(nlines):
            in1, in2, colour, l = sget("4i", pagesfile)
            if fieldname == b"Distribution" and j>1 and in1 > prev_indent:
                fieldval.pop()
            fieldval.append(sget(str(l)+"s", pagesfile)[0].decode("cp850"))
            prev_indent = in1
        taxonfields[fieldname.decode("cp850")] = fieldval[1:]
    pagesfile.read(4) #AnJz
    numrefs, = sget("i",pagesfile)
    if numrefs:
        taxonfields["refs"] = []
    for i in range(numrefs):
        mystery, l = sget("2i", pagesfile)
        authors, year, l = sget(str(l)+"s 2i", pagesfile)
        title, = sget(str(l)+"s", pagesfile)
        ref = {"authors":authors.decode("cp850"), "year":year,\
                "title":title.decode("cp850")}
        taxonfields["refs"].append(ref)
        
    accname = taxonfields.pop("Accepted Name")[0].partition(" {")[0]
    print(accname)
    taxon = taxonome.Taxon(taxonome.taxa.name_from_text(accname))
    if "Synonyms" in taxonfields:
        for syn in taxonfields.pop("Synonyms"):
            synname = taxonome.taxa.name_from_text(syn.partition(" {")[0])
            taxon.othernames.add(synname)
    taxon.extra_distribution = set()
    if "Distribution" in taxonfields:
        for region in taxonfields.pop("Distribution"):
            region = striprefs(region)
            statusbit = statusre.search(region)
            if not statusbit:
                taxon.distribution.add(region)
                continue
            ixbrack = statusbit.start()
            status = statusbit.group(1)
            if ": " in region: #Specified subregions
                for subreg in region.partition(": ")[2].strip("()").split(", "):
                    if status == "N":
                        taxon.distribution.add(subreg.strip())
                    else:
                        taxon.extra_distribution.add(subreg.strip())
            else: # Whole country
                if status == "N":
                    taxon.distribution.add(region[:ixbrack -1])
                else:
                    taxon.extra_distribution.add(region[:ixbrack -1])
    if "References" in taxonfields:
        del taxonfields["References"]
    if "Life Form" in taxonfields:
        lf = taxonfields["Life Form"][0].split("; ")
        taxonfields["Life Form"] = [a.partition(" {")[0] for a in lf]
    taxon.info = taxonfields
    taxa.add(taxon)
    pagesfile.read(4)
    # Of these three fields, the first seems to be indexing references,
    # the second is mapping the distribution (country number, native-ness).
    # The third usually seems to be empty.
    while True:
        if pagesfile.read(1) == b"J":
            if pagesfile.read(1) == b"z":
                break
    l, = sget("i",pagesfile)
    pagesfile.read(l*5)
    pagesfile.read(4)
    l, = sget("i",pagesfile)
    if l != 0:
        print(l, taxon)
        assert False
    if len(pagesfile.read(4)) < 4:
        break
        
pagesfile.close()