from cx_Freeze import setup, Executable, build_exe
import sys
import os.path

build_options = {'build_exe':
                    {'packages': ["lxml", "lxml.html"],
                     'includes': ["dbm.ndbm", "dbm.dumb"],
                     'excludes': ["tkinter"]
                    },
                'bdist_mac':
                    {'iconfile': "resources/leaf.icns",
                    },
                }

script = os.path.join("scripts", "taxonome")
if sys.platform == 'win32':
    exe = Executable(script, targetName="taxonome.exe", base="Win32GUI",
                        icon=os.path.join("resources", "favicon.ico"))
else:
    exe = Executable(script, targetName="taxonome")

setup(name = "Taxonome",
     version = "1.0",
     description = "Tools for working with taxonomic data",
     options = build_options,
     executables = [exe],
    )
