"""Script to upgrade cached user choices after changes to how names are stored.

Call with location of shelf as command line argument (without the .db suffix)
"""
import shelve
import sys

uc = shelve.open(sys.argv[1])

def convert_name1(name):
    name.g = name.nameparts[0]
    name.sp = name.nameparts[1]
    name.subnames = []
    if name.nameparts[2]:
        name.subnames.append(("subsp.", name.nameparts[2]))
    if name.nameparts[3]:
        name.subnames.append(("var.", name.nameparts[3]))

def convert_name(name):
    name.subnames = tuple(name.subnames)

total, failed = 0, 0
try:
    for name in uc.keys():
        try:
            prevoptions, prevchoice = uc[name]
        except Exception as e:
            print(name, e)
            failed += 1
        else:
            if not isinstance(prevchoice, Exception):
                convert_name(prevchoice[0])
                convert_name(prevchoice[1])
            for cname, aname in prevoptions:
                convert_name(cname)
                convert_name(aname)
            uc[name] = prevoptions, prevchoice
        total += 1

finally:
    uc.close()

print("Found {}, errors converting {}.".format(total, failed))
