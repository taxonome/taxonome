import sys
from cx_Freeze import setup, Executable

general_attrs = dict(author = "Thomas Kluyver",
                     author_email = "takowl@gmail.com",
                     url = "https://taxonome.bitbucket.io/",
                     classifiers = [
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3",
        "Topic :: Scientific/Engineering :: Bio-Informatics",
        ]
                    )
                    
base = None
if sys.platform == "win32":
    base = "Win32GUI"

setup(name = "Name splitter",
      version = "0.b7",
      description = "Splits authority names from species names in a CSV file.",
      executables = [Executable("cx_scripts/split_names_csv.py", base=base)],
      **general_attrs)
