import csv
import os.path
from copy import copy
import taxonome

from easygui import choicebox, fileopenbox, filesavebox, msgbox, exceptionbox

def main():
    fin = fileopenbox("Select a CSV file to process.", default="*.csv")
    if fin is None: return
    csvin = csv.DictReader(open(fin))

    namecol = choicebox(("Which column contains the species names & authorities "
                         "you want to split?") , title="Choose column",
                        choices=csvin.fieldnames)

    newheaders = copy(csvin.fieldnames)
    namecolix = newheaders.index(namecol)
    newnamecol, newauthcol = namecol+"_name", namecol+"_authority"
    newheaders[namecolix:namecolix+1] = [newnamecol, newauthcol]

    while True:
        fout = filesavebox(("Select a destination file. Don't overwrite the "
                              "original"), default="*.csv")
        if fout is None: return
        if not os.path.splitext(fout)[1]:
            fout = fout+'.csv'
        
        if fout != fin:
            break
            
        msgbox(("Overwriting the original file will not work. You need to save "
                "the file under a new name."), title="Error")
    
    csvout = csv.DictWriter(open(fout, "w"), fieldnames=newheaders)
    csvout.writeheader()
    
    count = 0
    for row in csvin:
        rawname = row.pop(namecol)
        name = taxonome.Name.from_string(rawname)
        row[newnamecol] = name.plain
        row[newauthcol] = str(name.authority)
        csvout.writerow(row)
        count += 1
    
    msgbox("Split %d names, saved to %s." % (count, fout), title="Finished")

if __name__ == "__main__":
    try:
        main()
    except Exception:
        exceptionbox()
