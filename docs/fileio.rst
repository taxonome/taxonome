.. _fileio:

Loading and saving data
=======================

There is no widely-adopted standard format for taxonomic data. Taxonome can
handle the two formats below, but if your data is in another format, you'll need
to convert it, or write some code to tell taxonome how to load it.

.. seealso:: :ref:`customreaders`

CSV files
---------

CSV is a simple, common format for many sorts of data, and can easily be created
by any spreadsheet program. Taxonome can load taxa from CSV files which include
scientific names, and optionally also the authorities for those names. Other
columns will be read as information on the species.

Taxonome can also save data to CSV files for further analysis (e.g. plotting
data).

Taxonome understands three main structures of CSV files:

* **Taxa records**: Each row refers to one taxon (by name).
* **Synonym records**: Each row refers to one name, and contains both that name,
  and the accepted name which it matches. Accepted names may appear with the same
  name for both parts, or can be taken from the relevant fields of their
  synonyms.
* **Individual records**: Each row refers to one individual specimen, sighting,
  population, or similar, of a particular taxon. A taxon name may appear on one
  or more rows.

In all cases, the authorities for the scientific names can be in a separate column,
present within the names column, or absent.

.. seealso:: API reference for the :mod:`taxonome.taxa.file_csv` module.

Taxonome JSONlines files
------------------------

Taxonome defines a more flexible file format. This can handle things like a list of
measured heights for each species in a dataset.

.. seealso::

   Module :mod:`taxonome.taxa.file_jsonlines`
     API referenec for the :mod:`~taxonome.taxa.file_jsonlines` module.
    
   `JSON.org <http://json.org/>`_
     Overview of JSON data format.
