.. _customreaders:

Reading specific data sources
=============================

At present, to use these tools, you must have Taxonome installed as a Python
package (not just the application). I may make standalone versions in the future.

I hope that more tools will be added to this page as people use Taxonome with
other data sources. If you write a script to read a particular data source,
please get in touch so that it can be listed.

.. _reader_kew_grass:

Kew grass synonymy
------------------

This requires `MDB tools <http://mdbtools.sourceforge.net/>`_, which at the time
of writing is avaible for Linux and Mac OS X.

* Download and unzip the `database from Kew's website <http://www.kew.org/data/grasses-syn/download.htm>`_.
* Download the `necessary files to read it <https://bitbucket.org/taxonome/taxonome/src/default/custom_readers/kew_grass_synonymy>`_
  into one folder.
* Run ``./extract_grasssyn.sh grassFlora.mdb``

ILDIS legume database
---------------------

* Download the `ILDIS Explorer <http://www.ildis.org/cd/Explorer/>`_ application,
  and find the ``Pages.dtx`` data file.
* Download the `script to read it <https://bitbucket.org/taxonome/taxonome/raw/default/custom_readers/ildis_extract.py>`_.
* Run ``python3 ildis_extract.py Pages.dtx``
