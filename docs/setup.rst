Installing Taxonome
===================

Latest release: **1.5** (16 July 2014)

+-----------------------------------------------------------------------------+
| |windownload|_ for Windows (.exe)                                           |
+-----------------------------------------------------------------------------+
| |macdownload|_ for Mac OS X (.dmg) (v 1.0)                                  |
+-----------------------------------------------------------------------------+
| |ppa|_ for Ubuntu.                                                          |
+-----------------------------------------------------------------------------+
| `On PyPI <http://pypi.python.org/pypi/Taxonome>`_ as a Python package.      |
+-----------------------------------------------------------------------------+

.. I want a bold link, but rST can't (yet?) do nested markup. Hence this hack:
.. |windownload| replace:: **Download**
.. _windownload: https://bitbucket.org/taxonome/taxonome/downloads/Taxonome_1.5.exe
.. |macdownload| replace:: **Download**
.. _macdownload: https://bitbucket.org/taxonome/taxonome/downloads/Taxonome-1.0.dmg
.. |ppa| replace:: **In a PPA**
.. _ppa: https://launchpad.net/~takluyver/+archive/taxonome

The downloads above include everything you need to use Taxonome as an application.

-----

If you want to run Taxonome from the source code instead, you'll need:

* `Python <http://www.python.org/>`_ 3
* PyQt4 or PySide (for the GUI)
* BeautifulSoup4 & lxml (for web services)
* suds (`Python 3 version <https://bitbucket.org/bernh/suds-python-3-patches/downloads>`_,
  for WoRMS web service).

Get the `source code from Bitbucket <https://bitbucket.org/taxonome/taxonome>`_.
If you want to use the GUI, run ``python uic2.py``. Then run
``python setup.py install`` to install Taxonome.
