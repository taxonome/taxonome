Regions
=======

:mod:`regions` module
---------------------

.. automodule:: taxonome.regions

TDWG Data
---------

.. autofunction:: load_tdwg

.. autofunction:: find_tdwg

.. autofunction:: tdwgise

Map class
---------

.. autoclass:: Map
