File input and output
=====================

CSV files
---------

.. automodule:: taxonome.taxa.file_csv
   :members:


JSONlines files
---------------

.. automodule:: taxonome.taxa.file_jsonlines
   :members:
