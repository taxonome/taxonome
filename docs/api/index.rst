Taxonome API
============

.. toctree::
   :maxdepth: 2
   
   taxa.rst
   regions.rst
   fileio.rst
