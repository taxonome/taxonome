Taxa
====

Taxon class
-----------

.. autoclass:: taxonome.Taxon


Name class
----------

.. autoclass:: taxonome.Name


Collections of taxa
-------------------

.. autoclass:: taxonome.TaxonSet
   :show-inheritance:

.. autoclass:: taxonome.taxa.collection.TaxaResource
   :members: select, resolve_name, get_by_accepted_name


Matching and combining datasets
-------------------------------

.. autofunction:: taxonome.taxa.match_taxa

.. autofunction:: taxonome.taxa.combine_datasets
