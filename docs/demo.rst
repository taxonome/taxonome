API Demonstration
=================

This is an example of how the Taxonome API can be used. To follow it, you will
need `Python 3 <http://python.org/>`_ installed, and Taxonome installed as a
package. If you are new to Python, you might want to go through a tutorial first
- the `official Python tutorial <http://docs.python.org/py3k/tutorial/index.html>`_
is one of several online. You can enter the commands below in an interactive
Python session.

First, we import the package::

    import taxonome

Create a container to hold a group of taxa::

    a_genus = taxonome.TaxonSet()

Create two species (these are actually liquorice plants)::

    sp1 = taxonome.Taxon("Glycyrrhiza glabra", "L.")
    sp1.othernames.add(taxonome.Name("Glycyrrhiza glandulifera", "Waldst. & Kit."))
    
    sp2 = taxonome.Taxon("Glycyrrhiza uralensis", "Fisch.")
    sp2.othernames.add(taxonome.Name("Glycyrrhiza glandulifera", "Ledeb."))

Add them to the container::

    a_genus.add(sp1)
    a_genus.add(sp2)

Now, what happens if we ask for an ambiguous name?

::

    a_genus.select("Glycyrrhiza glandulifera")

You should see::

    == Select a match for Glycyrrhiza glandulifera: ==
    N (Reject all)
    0 (Specify new name to search for)
    1 Glycyrrhiza glandulifera Waldst. & Kit. 	  -> Glycyrrhiza glabra L.
    2 Glycyrrhiza glandulifera Ledeb. 	  -> Glycyrrhiza uralensis Fisch.
    Enter a number:

This is just a small sample of what Taxonome can do; read on for more
information.
