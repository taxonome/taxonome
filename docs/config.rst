Configuration
=============

Taxonome can be configured with an INI style configuration file, located at
:file:`~/.taxonome/taxonome.cfg`, which you can change with a text editor. The
default options are::

    [main]
    # Threshold for matching differently spelled species names, between 0
    # (anything matches) and 1 (exact matches only).
    name-fuzzy-threshold = 0.85
    # Threshold for matching species authorities.
    author-fuzzy-threshold = 0.6
    # Load the TDWG regions when Taxonome is loaded?
    load-tdwg-regions = True
    # HTTP user agent for making API calls.
    user-agent = Taxonome (in testing)

    [cache]
    location = ~/.taxonome
    expiry-days = 30
    # Clean old entries from caches when Taxonome is loaded? (Currently unused)
    clean-on-load = True
    # This is the location to store choices made by the user when selecting
    # ambiguous names.
    user-choices = %(location)s/user_choices

    [api-keys]
    # Programmatic users have to request their own API key
    tropicos = 
