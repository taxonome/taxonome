Taxonome
========

.. image:: _static/gui_screenshot.png
   :align: right

Taxonome is a tool to handle data about species, organised by biological names.
In particular, it allows matching of species names from different sources, and
standardisation of distribution information to `TDWG regions <http://www.kew.org/gis/tdwg/>`_.

Taxonome has both a graphical user interface for normal use, and an advanced
programmatic interface using the `Python <http://python.org/>`_ programming language.

Contents:

.. toctree::
   :maxdepth: 2
   
   setup.rst
   gui_manual.rst
   concepts.rst
   fileio.rst
   customreaders.rst
   distributions.rst
   config.rst
   demo.rst
   api/index.rst

Data
====

.. toctree::

   grasses_ptype

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

