Grass species photosynthetic type
=================================

This is a dataset from Osborne et al 2014 (in review), classifying grass species
as C3 or C4.

`Download dataset <https://www.dropbox.com/s/pc9z0n1dzxk5y0c/Kew_grass_ptype.jsonlines>`_

The names are based on the `Kew grass synonymy database <http://www.kew.org/data/grasses-syn/index.htm>`_,
using the version created on 25 September 2013, extracted using the :ref:`Kew Grasses
custom reader <reader_kew_grass>`. The names were matched against
the tables in the paper. Most names only needed to be matched to a genus (table
S3 in the paper), the remainder were matched to species or even subspecies level
(table 1 and table S4). Unmatched names which had uncertain status (codes X and
Z in the database, mostly nomen nudum and names with uncertain application), or
were intergeneric hybrids (hybrid code G) were disregarded.

Using the dataset
-----------------

:doc:`Install Taxonome <setup>`, launch it, and select File, Load from JSONlines.
Select the file. Once it has loaded, you should be able to search by species using
the search box in the top right. Use wildcards to search genera, e.g. 'Panicum \*'.
The results include names matched by synonyms. You can match up other datasets
to this information using the :guilabel:`&Taxa`, :guilabel:`Match taxa by name...`
menu option.

You can also use the dataset programmatically, using :func:`taxonome.taxa.file_jsonlines.load_taxa`
or :func:`~taxonome.taxa.file_jsonlines.iter_taxa`.
